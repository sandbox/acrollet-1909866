/**
 * @file
 *
 * Javascript functions for library_hours module
 *
 * @author Sean Watkins <slwatkins@uh.edu>
 * @copyright 2011 University of Houston Libraries (http://info.lib.uh.edu)
 */

library_hours = {};

library_hours.tab = function(id){
    $('#library_hours-periods-tabs li').removeClass('selected');
    $('#library_hours-periods-tabs #library_hours-tab-' + id).addClass('selected');

    $('#library_hours-periods .library_hours-view').removeClass('selected');
    $('#library_hours-periods #library_hours-period-' + id).addClass('selected');
}

library_hours.location = function(id){
     window.location = Drupal.settings.basePath + 'hours/' + id;
}
