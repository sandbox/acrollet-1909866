<?php

/**
 * @file
 * Theme for patron hours display for the library_hours module
 *
 * @author Sean Watkins <slwatkins@uh.edu>
 * @copyright 2011 University of Houston Libraries (http://info.lib.uh.edu)
 *
 * Available variables:
 * - $lid: Location unique identifier
 * - $pid: Period unique identifier
 * - $locations: Array of available locations
 * - $periods: Array of period information for a selected location
 * - $description: Location description string
 *
 * @see template_preprocess()
 * @see template_preprocess_hours_display()
 */
?>
<div id="library_hours-content">
	<div id="library_hours-locations">
		<ul id="library_hours-location-list">
			<?php foreach($locations as $location): ?>
				<li class="library_hours-location <?php echo (($location['lid'] == $lid) ? 'selected' : '') ?>" onclick="javascript:library_hours.location('<?php echo $location['lid'] ?>')"><?php echo $location['name'] ?></li>
				<?php foreach($location['children'] as $child): ?>
					<li class="library_hours-location library_hours-child <?php echo (($child['lid'] == $lid) ? 'selected' : '') ?>" onclick="javascript:library_hours.location('<?php echo $child['lid'] ?>')">- <?php echo $child['name'] ?></li>
				<?php endforeach; ?>
			<?php endforeach; ?>
		</ul>
	</div>
	<div id="library_hours-periods">
		<ul id="library_hours-periods-tabs">
			<?php foreach($periods as $period): ?>
				<li id="library_hours-tab-<?php echo $period['pid'] ?>" class="<?php echo (($period['pid'] == $pid) ? 'selected' : '') ?>" onclick="javascript:library_hours.tab('<?php echo $period['pid'] ?>')">
					<div class="library_hours-name"><?php echo $period['name'] ?></div>
					<div class="library_hours-daterange"><?php echo date("M j", $period['from_date']) ?> - <?php echo date("M j", $period['to_date']) ?></div>
				</li>
			<?php endforeach; ?>
		</ul>
		<?php foreach($periods as $period): ?>
			<div id="library_hours-period-<?php echo $period['pid'] ?>" class="library_hours-view <?php echo (($period['pid'] == $pid) ? 'selected' : '') ?>">
				<div class="library_hours-stdhours">
					<div class="library_hours-name"><?php echo $period['name'] ?> Hours</div>
					<ul>
						<?php for($i = 0; $i < count($period['hours']); $i++): ?>
							<li class="library_hours-hour"><div class="library_hours-dow"><?php echo _library_hours_dow($i) ?>:</div><div class="library_hours-time"><?php echo $period['hours'][$i] ?></div></li>
						<?php endfor; ?>
					</ul>
				</div>
				<?php if(count($period['exceptions']) > 0): ?>
				<div class="library_hours-exceptions">
					<div class="library_hours-name"><?php echo t('Exceptions') ?></div>
					<div class="library_hours-hour">
						<ul>
							<?php foreach($period['exceptions'] as $exception): ?>
								<li class="library_hours-label"><?php echo $exception['name'] ?></li>
								<?php foreach($exception['times'] as $time): ?>
									<li class="library_hours-time"><?php echo $time ?></li>
								<?php endforeach; ?>
							<?php endforeach; ?>
						</ul>
					</div>
				</div>
				<?php endif; ?>
				<div class="library_hours-locationdescription"><?php echo $description ?></div>
				<div class="library_hours-disclaimer"><?php echo t('Library hours are subject to change') ?></div>
			</div>
		<?php endforeach; ?>
	</div>
</div>
